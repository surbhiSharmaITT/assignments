﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tech_Learning
{
    /// <summary>
    /// Main Console Exceution invoking all the assessments
    /// </summary>
    class ExecutionFile
    {
        static void Main(string[] args)
        {
            //Assessment 3 for Exception Handling,File Handling and Delegates
            // ExceptionAndDelegatesManipulation.ImplementExceptionAndDelegatesManipulation();
            //Assessment 4 for Enums
            //Enums.Assessment4();
            //Assessment 5 for File Handling
            FileHandling.FileHandlingExecution();
        }
    }
}
