﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tech_Learning
{
    /// <summary>
    /// DEclaring Enum for Property Gender
    /// </summary>
    public enum Gender
    {
        Unknown,
        Male,
        Female
    }

}
