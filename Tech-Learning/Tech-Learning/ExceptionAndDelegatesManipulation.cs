﻿using System;
using System.IO;

namespace Tech_Learning
{
    class ExceptionAndDelegatesManipulation 
    {
        /// <summary>
        /// Method which implements exception handling and delgates use for storing the employee details who 
        /// all are eligible for promotion according to their experience
        /// </summary>
        public static void ImplementExceptionAndDelegatesManipulation()
        {
            StreamReader streamReader = null;
            try
            {
                
                //Calling method which will take input from user
                EmployeeInputFromUser.InputFromUserMethod();

                //Reading the details from file
                streamReader = new StreamReader(Constants.FilePath);
                Console.WriteLine(streamReader.ReadToEnd());
            }
            catch (FileNotFoundException e)
            {
                //To Catch file not found exception
                Console.WriteLine("{0} file does not exists", e.FileName);
            }
            catch (Exception e)
            {
                //To catch other exception if present
                Console.WriteLine(e.Message);
            }
            //finally
            //{
            //    //To clean resources
            //    if (streamReader != null)
            //    {
            //        streamReader.Close();
            //    }
            //    Console.WriteLine("Finally block executed. All resources are clean and free now.");
            //}
        }

  
    }
}
