﻿using System;
using System.Collections.Generic;
using System.IO;


namespace Tech_Learning
{
    delegate bool IsPromotable(Employee employee);
    class Employee
    {
        /// <summary>
        /// Read- write property of employee
        /// </summary>
        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
        public int Experience { get; set; }

        /// <summary>
        /// Invoking the method PromoteEmployee which will take delegate and list as a parameter
        /// It will write the employee details of all those employees who are eligible for promotion
        /// </summary>
        /// <param name="employeeList">List of Employee details to be checked for promotion</param>
        /// <param name="isEligibleToPromote">Delegate used for validating the condition</param>
        public static void PromoteEmployee(IList<Employee> employeeList, IsPromotable isEligibleToPromote)
        {
            StreamWriter streamWriter = null;
            try
            {
                streamWriter = new StreamWriter(Constants.FilePath);

                foreach (Employee employee in employeeList)
                {
                    if (isEligibleToPromote(employee))
                    {
                        streamWriter.WriteLine("{0} {1} {2} {3}", employee.Id, employee.Name, employee.Salary,
                            employee.Experience);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                streamWriter.Close();
            }
        }
    }
}
