﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tech_Learning
{
    /// <summary>
    /// Defining read-write property for Customer
    /// </summary>
    class Customer
    {
        public string Name { get; set; }
        public Gender Gender { get; set; }
    }
}
