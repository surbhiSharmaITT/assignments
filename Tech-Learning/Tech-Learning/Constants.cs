﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tech_Learning
{
    /// <summary>
    /// Consists of all the constants used in the Solution
    /// </summary>
    class Constants
    {
        public const string ExceptionStringDelimiterNotFound = "Enter string separated by |";
        public const string NumberOfEmployeePromptString = "Enter number of employees: ";
        public const string EmployeeDetailPromptString = "Employee details separated by |:";
        public const string FilePath = @"H:\DataFile.txt";
    }
}
