﻿using System;
using System.Collections.Generic;


namespace Tech_Learning
{

    class EmployeeInputFromUser
    {
        /// <summary>
        /// Method which will take employee details from the user and use delegates for verification whether the employee
        /// is eligible for promotion or not on the basis of experience
        /// </summary>
        public static void InputFromUserMethod()
        {
            //Taking input from user about number of employees
            Console.WriteLine(Constants.NumberOfEmployeePromptString);
            int numberOfEmployees = Convert.ToInt32(Console.ReadLine());
          
            IList<Employee> employeeList = new List<Employee>();
            Console.WriteLine(Constants.EmployeeDetailPromptString);

            for (int counter = 0; counter < numberOfEmployees; counter++)
            {
                string detailsString = Console.ReadLine();
                if (detailsString != null && !detailsString.Contains("|"))
                {
                    throw new Exception(Constants.ExceptionStringDelimiterNotFound);
                }
                if (detailsString != null)
                {
                    string[] detailsArray = detailsString.Split('|');
                    Employee employee = new Employee()
                    {
                        Id = Convert.ToInt32(detailsArray[0]),
                        Name = detailsArray[1],
                        Experience = Convert.ToInt32(detailsArray[2]),
                        Salary = Convert.ToInt32(detailsArray[3])
                    };
                    employeeList.Add(employee);
                }
            }

            //Invoking delegates by making its object
            IsPromotable isPromotable = new IsPromotable(Promote);
            Employee.PromoteEmployee(employeeList, isPromotable);
        }

        //Method definition to be used by delegate for verifying the eligibility of the promotion of candidate
        public static bool Promote(Employee employee)
        {
            if (employee.Experience >= 3)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
