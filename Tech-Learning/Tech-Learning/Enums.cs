﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tech_Learning
{
    class Enums
    {
        /// <summary>
        /// Invoke method for Assessment 4 based on Enums
        /// </summary>
        public static void Assessment4()
        {
            //Declare arrays of customers
            Customer[] customers = new Customer[3];

            //Initialize the customer array with values
            customers[0] = new Customer
            {
                Gender = Gender.Male,
                Name = "Mark"
            };
            customers[1] = new Customer
            {
                Gender = Gender.Female,
                Name = "Mary"
            };
            customers[2] = new Customer
            {
                Gender = Gender.Unknown,
                Name = "Janister"
            };

            //Print the customer details
            foreach (Customer customer in customers)
            {
                Console.WriteLine("Customer Name :{0} Gender:{1}",customer.Name,GetGender(customer.Gender));
            }
        }
        /// <summary>
        /// Method to return type of gender with respect to the enum value passed
        /// </summary>
        /// <param name="gender">Enum of type Gender</param>
        /// <returns>Return the corresponding choice wrt Enum value</returns>
        public static string GetGender(Gender gender)
        {
            switch (gender)
            {
                case Gender.Unknown:
                    return "Unknown";
                case Gender.Male:
                    return "Male";
                case Gender.Female:
                    return "Female";
                  default:
                      return "Invalid entry";
            }
        }
    }
}
