﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class Constants
    {
        public const string LineSeparator = "----------------------------------------";
        public const string LogFileName = "Exception";
        public const string IdString = "Id";
        public const string EmployeeTypeString = "EmployeeType";
    }
}
