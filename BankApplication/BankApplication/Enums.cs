﻿namespace BankApplication
{
    class Enums
    {
        //Specifies the type of transaction whether deposit or withdrawl
        public enum TypeOfTransaction
        {
            Deposit = 1,
            Withdraw = 2
        }
    }
}
