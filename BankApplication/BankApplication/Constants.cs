﻿namespace BankApplication
{
    class Constants
    {
        public const string DirectoryPath = @"H:\Programs\Text Files";
        public const decimal MaxLimit = 100000;
        public const decimal MinLimit = 0;
        public const string CS = "DBCS";
    }
}
