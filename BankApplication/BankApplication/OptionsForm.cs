﻿using System;
using System.Windows.Forms;

namespace BankApplication
{
    public partial class OptionsForm : Form
    {
        public OptionsForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// For directing to windows performing Insert operation
        /// </summary>
        private void InsertNewCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                InsertForm objectForm = new InsertForm();
                objectForm.Show();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
            
        }

        /// <summary>
        /// For directing to windows performing Update operation
        /// </summary>
        private void UpdateUserInformation_Click(object sender, EventArgs e)
        {
            this.Hide();
            UpdateForm objectForm = new UpdateForm();
            objectForm.Show();
        }

        /// <summary>
        /// For directing to windows performing Delete operation
        /// </summary>
        private void DeleteCustomer_Click(object sender, EventArgs e)
        {
            this.Hide();
            DeleteForm objectForm = new DeleteForm();
            objectForm.Show();
        }
    }
}
