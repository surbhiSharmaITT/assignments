﻿using System;
using System.IO;
using System.Windows.Forms;
using BankApplication.DAL.Model;

namespace BankApplication
{
    class Search : Account
    {
        /// <summary>
        /// Search on the basis of ID on all the files present in a directory.
        /// If present then return true. Else return false
        /// </summary>
        /// <param name="customer">Whose data is to be added</param>
        /// <returns>Return if the file is found or not</returns>
        public virtual bool SearchFile(Customer customer)
        {
            string[] filePaths = Directory.GetFiles(Constants.DirectoryPath, "*.txt", SearchOption.AllDirectories);
            bool result = false;
            foreach (string filePath in filePaths)
            {
                using (StreamReader streamReader = new StreamReader(filePath))
                {
                    string readContents = streamReader.ReadToEnd();
                    string[] fileContent = readContents.Split(' ');
                    if (fileContent.Length > 1)
                    {
                        int fileId = Convert.ToInt32(fileContent[0]);
                        if (fileId == customer.Id)
                        {
                            Search.FilePath = filePath;
                            result = true;
                            MessageBox.Show("File Already Present.");
                            break;
                        }
                    }

                }
            }
            return result;
        }
    }
}
