﻿namespace BankApplication
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InsertNewCustomer = new System.Windows.Forms.Button();
            this.UpdateUserInformation = new System.Windows.Forms.Button();
            this.DeleteCustomer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // InsertNewCustomer
            // 
            this.InsertNewCustomer.Location = new System.Drawing.Point(35, 34);
            this.InsertNewCustomer.Name = "InsertNewCustomer";
            this.InsertNewCustomer.Size = new System.Drawing.Size(177, 23);
            this.InsertNewCustomer.TabIndex = 0;
            this.InsertNewCustomer.Text = "Insert New Customer";
            this.InsertNewCustomer.UseVisualStyleBackColor = true;
            this.InsertNewCustomer.Click += new System.EventHandler(this.InsertNewCustomer_Click);
            // 
            // UpdateUserInformation
            // 
            this.UpdateUserInformation.Location = new System.Drawing.Point(35, 89);
            this.UpdateUserInformation.Name = "UpdateUserInformation";
            this.UpdateUserInformation.Size = new System.Drawing.Size(177, 23);
            this.UpdateUserInformation.TabIndex = 1;
            this.UpdateUserInformation.Text = "Update Customer";
            this.UpdateUserInformation.UseVisualStyleBackColor = true;
            this.UpdateUserInformation.Click += new System.EventHandler(this.UpdateUserInformation_Click);
            // 
            // DeleteCustomer
            // 
            this.DeleteCustomer.Location = new System.Drawing.Point(35, 142);
            this.DeleteCustomer.Name = "DeleteCustomer";
            this.DeleteCustomer.Size = new System.Drawing.Size(177, 23);
            this.DeleteCustomer.TabIndex = 2;
            this.DeleteCustomer.Text = "DeleteCustomer";
            this.DeleteCustomer.UseVisualStyleBackColor = true;
            this.DeleteCustomer.Click += new System.EventHandler(this.DeleteCustomer_Click);
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(237, 261);
            this.Controls.Add(this.DeleteCustomer);
            this.Controls.Add(this.UpdateUserInformation);
            this.Controls.Add(this.InsertNewCustomer);
            this.Name = "OptionsForm";
            this.Text = "Options";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button InsertNewCustomer;
        private System.Windows.Forms.Button UpdateUserInformation;
        private System.Windows.Forms.Button DeleteCustomer;
    }
}