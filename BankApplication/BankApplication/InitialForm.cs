﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankApplication
{
    public partial class InitialForm : Form
    {
        public InitialForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// For directing to Database handling
        /// </summary>
        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            OptionsForm formObject = new OptionsForm();
            formObject.Show();
        }

        /// <summary>
        /// For directing to file handling windows form
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            CustomerForm formObject = new CustomerForm();
            formObject.Show();
        }
    }
}
