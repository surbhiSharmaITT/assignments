﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace BankApplication
{
    public partial class DeleteForm : Form
    {
        public DeleteForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Factory b = getIntance();
                b.
                //Read the connection string from App.Config file
                string ConnectionString = ConfigurationManager.ConnectionStrings[Constants.CS].ConnectionString;
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    //Create the SqlCommand object
                    SqlCommand cmd =
                        new SqlCommand("spAllProcedure", con) {CommandType = System.Data.CommandType.StoredProcedure};
                    //Specify that the SqlCommand is a stored procedure

                    //Add the input parameters to the command object
                    cmd.Parameters.AddWithValue("@Action", "Delete");
                    cmd.Parameters.AddWithValue("@U_CustomerId", IdTextBox.Text);

                    //Open the connection and execute the query
                    con.Open();
                    cmd.ExecuteNonQuery();

                    ResultLabel.Text = "Deleted";
                }
            }
            catch (Exception exception)
            {
                ResultLabel.Text = exception.Message;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
