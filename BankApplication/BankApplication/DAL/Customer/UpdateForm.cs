﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace BankApplication
{
    public partial class UpdateForm : Form
    {
        public UpdateForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //Read the connection string from Web.Config file
                string ConnectionString = ConfigurationManager.ConnectionStrings[Constants.CS].ConnectionString;
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    //Create the SqlCommand object
                    SqlCommand cmd =
                        new SqlCommand("spAllProcedure", con) {CommandType = System.Data.CommandType.StoredProcedure};

                    //Specify that the SqlCommand is a stored procedure

                    //Add the input parameters to the command object
                    cmd.Parameters.AddWithValue("@Action", "Update");
                    cmd.Parameters.AddWithValue("@U_CustomerId", IdTextBox.Text);
                    cmd.Parameters.AddWithValue("@U_Name", IdTextBox.Text);
                    cmd.Parameters.AddWithValue("@U_Gender", NameTextBox.Text);
                    cmd.Parameters.AddWithValue("@U_EmailId", EmailTextBox.Text);
                   
                    //Open the connection and execute the query
                    con.Open();
                    cmd.ExecuteNonQuery();
                    
                    ResultLabel.Text = "Updated";
                }
            }
            catch (Exception exception)
            {
                ResultLabel.Text = exception.Message;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
