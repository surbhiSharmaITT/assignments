﻿using System;
using System.Windows.Forms;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BankApplication
{
    public partial class InsertForm : Form
    {
        public InsertForm()
        {
            InitializeComponent();
        }

       /// <summary>
       /// comment?
       /// 
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        private void InsertButton_Click(object sender, EventArgs e)
        {
            try
            {
                #region LocalVariables

                string address = AddressTextBox.Text;
                string houseNo = string.Empty; 
                string street = string.Empty;
                string city = string.Empty; 
                string postalCode = string.Empty; ;
                string country = string.Empty; ;
                int addressId = 0; 
                int customerId = 0;
                DateTime dateTime = DateTime.Now;
                //string date =dateTime.ToString("dd/MM/yyyy");
                //decimal balance = 0;


                #endregion

                if (address != null)
                {
                    string[] addressParts = address.Split(',');
                    houseNo = addressParts[0];
                    street = addressParts[1];
                    city = addressParts[2];
                    postalCode = addressParts[3];
                    country = addressParts[4];
                }
                //Read the connection string from App.Config file
                string connectionString = ConfigurationManager.ConnectionStrings[Constants.CS].ConnectionString;
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                     //Create the SqlCommand object 
                    SqlCommand cmd =
                        new SqlCommand("spAddAddress", con) {CommandType = System.Data.CommandType.StoredProcedure};
                    //Specify that the SqlCommand is a stored procedure

                    //Add the input parameters to the command object
                    cmd.Parameters.AddWithValue("@HouseNo", houseNo);
                    cmd.Parameters.AddWithValue("@Street", street);
                    cmd.Parameters.AddWithValue("@City", city);
                    cmd.Parameters.AddWithValue("@PostalCode", postalCode);
                    cmd.Parameters.AddWithValue("@Country", country);

                    //Add the output parameter to the command object
                    SqlParameter outPutParameter = new SqlParameter
                    {
                        ParameterName = "@AddressId",
                        SqlDbType = System.Data.SqlDbType.Int,
                        Direction = System.Data.ParameterDirection.Output
                    };
                    cmd.Parameters.Add(outPutParameter);

                    //Open the connection and execute the query
                    con.Open();
                    cmd.ExecuteNonQuery();

                    //Retrieve the value of the output parameter
                    addressId = Convert.ToInt32(outPutParameter.Value.ToString());
                 
                }

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    //Create the SqlCommand object
                    SqlCommand cmd =
                        new SqlCommand("spAddCustomer", con) {CommandType = System.Data.CommandType.StoredProcedure};

                    //Specify that the SqlCommand is a stored procedure

                    //Add the input parameters to the command object
                    cmd.Parameters.AddWithValue("@Name", NameTextBox.Text);
                    cmd.Parameters.AddWithValue("@Gender", GenderTextBox.Text);
                    cmd.Parameters.AddWithValue("@EmailId", EmailIdTextBox.Text);
                    cmd.Parameters.AddWithValue("@AddressId", addressId);

                    //Add the output parameter to the command object
                    SqlParameter outPutParameter = new SqlParameter
                    {
                        ParameterName = "@CustomerId",
                        SqlDbType = System.Data.SqlDbType.Int,
                        Direction = System.Data.ParameterDirection.Output
                    };
                    cmd.Parameters.Add(outPutParameter);

                    //Open the connection and execute the query
                    con.Open();
                    cmd.ExecuteNonQuery();

                    //Retrieve the value of the output parameter
                    string CustomerId = outPutParameter.Value.ToString();
                    ResultLabel.Text = "CustomerId = " + CustomerId;

                    customerId = Convert.ToInt32(outPutParameter.Value.ToString());
                }
                /*using (SqlConnection con = new SqlConnection(connectionString))
                {
                    //Create the SqlCommand object
                    SqlCommand cmd =
                        new SqlCommand("spAddAccount", con) {CommandType = System.Data.CommandType.StoredProcedure};
                    //Specify that the SqlCommand is a stored procedure

                    //Add the input parameters to the command object
                    cmd.Parameters.AddWithValue("@CustomerId", customerId);
                    cmd.Parameters.AddWithValue("@OpenedDate", dateTime);
                    cmd.Parameters.AddWithValue("@Balance", balance);

                    //Add the output parameter to the command object
                    SqlParameter outPutParameter = new SqlParameter
                    {
                        ParameterName = "@AccountNumber",
                        SqlDbType = System.Data.SqlDbType.Int,
                        Direction = System.Data.ParameterDirection.Output
                    };
                    cmd.Parameters.Add(outPutParameter);

                    //Open the connection and execute the query
                    con.Open();
                    cmd.ExecuteNonQuery();

                    //Retrieve the value of the output parameter
                    string AccountNumber = outPutParameter.Value.ToString();
                    ResultLabel2.Text = "Account Number" + AccountNumber;
                }*/
            }
            catch (Exception exception)
            {
                ResultLabel.Text = exception.StackTrace;
            }
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
