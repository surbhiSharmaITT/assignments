﻿
namespace BankApplication.Contracts
{
    interface IAccount
    {
        //Check the limit while transaction
        bool CheckLimit();

        //Completes the transaction
        void Transaction(decimal amount);
    }
}
