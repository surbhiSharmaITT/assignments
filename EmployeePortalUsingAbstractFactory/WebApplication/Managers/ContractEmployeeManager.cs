﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Managers
{
    public class ContractEmployeeManager : IEmployeeManager
    {
        /// <summary>
        /// Specifies the bonus for contract type of employee
        /// </summary>
        public decimal GetBonus()
        {
            return 500;
        }

        /// <summary>
        /// Specifies the hourly pay for contract type of employee
        /// </summary>
        public decimal GetPay()
        {
            return 1200;
        }

        /// <summary>
        /// Specifies the medical allowance for contract type of employee
        /// </summary>
        public decimal GetMedicalAllowance()
        {
            return 10000;
        }
    }
}