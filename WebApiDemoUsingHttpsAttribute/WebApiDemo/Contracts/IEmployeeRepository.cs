﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmployeeDataAccessLayer;

namespace WebApiDemo.Contracts
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetAllEmployees();
        IEnumerable<Employee> GetMaleEmployees();

        IEnumerable<Employee> GetFemaleEmployees();

        Employee GetEmployeeById(int id);

        void AddEmployee(Employee employee);

        void Save();

        void DeleteEmployee(Employee employee);
    }
}
