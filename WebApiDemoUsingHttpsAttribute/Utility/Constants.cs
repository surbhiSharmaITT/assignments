﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class Constants
    {
        public const string GenderOptionsAll = "all";
        public const string GenderOptionsMale = "male";
        public const string GenderOptionsFemale = "female";
        public const string GenderInvalidOptionError = "Value for gender must be Male, Female or All. ";
        public const string GenderInvalidInputError = " is invalid.";
        public const string LoadedIdNotFoundError = "Employee with Id not found. Id is ";
        public const string UpdatedIdNotFoundError = "Employee with Id not found to update. Id is ";
        public const string DeletedIdNotFoundError = "Employee with Id not found to delete. Id is ";
        public const int PortNumber = 44322;
        public const string ResponseContentPart2 = "text/html";
        public const string ResponseContentPart1 = "<p>Use https instead of http</p>";
    }
}
