﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class Constants
    {
        public const int PermanentInstructorTypeValue = 1;
        public const int ContractInstructorTypeValue = 2;
        public const string LineSeparation = "----------------------------------------";
        public const string LogFileName = "Exception";
        public const string DateFormat = "yyyyMMdd";
        public const string FullLogFileName = "{0}_{1}.log";
    }
}
