﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Managers
{
    public class PermanentInstructorManager : IInstructorManager
    {
        /// <summary>
        /// Specifies the bonus for permanent type of employee
        /// </summary>
        public decimal GetBonus()
        {
            return 1000;
        }

        /// <summary>
        /// Specifies the hourly pay for permanent type of employee
        /// </summary>
        public decimal GetPay()
        {
            return 800;
        }

        /// <summary>
        /// Specifies the house allowance for permanent type of employee
        /// </summary>
        public decimal GetHouseAllowance()
        {
            return 15000;
        }
    }
}