﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication.Managers
{
    public interface IInstructorManager
    {
        decimal GetBonus();
        decimal GetPay();
    }
}
