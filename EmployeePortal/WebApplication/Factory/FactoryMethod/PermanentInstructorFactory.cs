﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication.Models.ViewModels;
using WebApplication.Managers;

namespace WebApplication.Factory.FactoryMethod
{
    public class PermanentInstructorFactory : BaseInstructorFactory
    {
        public PermanentInstructorFactory(InstructorModel instructor)
            : base(instructor)
        {
        }

        public override IInstructorManager Create()
        {
            PermanentInstructorManager manager = new PermanentInstructorManager();
            _instructor.HouseAllowance = manager.GetHouseAllowance();
            return manager;
        }
    }
}