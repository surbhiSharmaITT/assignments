﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication.Models.ViewModels;
using WebApplication.Managers;

namespace WebApplication.Factory.FactoryMethod
{
    public abstract class BaseInstructorFactory
    {
        protected InstructorModel _instructor;
        public BaseInstructorFactory(InstructorModel instructor)
        {
            _instructor = instructor;
        }
        public InstructorModel ApplySalary()
        {
            IInstructorManager manager = this.Create();
            _instructor.Bonus = manager.GetBonus();
            _instructor.HourlyPay = manager.GetPay();
            return _instructor;
        }
        public abstract IInstructorManager Create();
    }
}