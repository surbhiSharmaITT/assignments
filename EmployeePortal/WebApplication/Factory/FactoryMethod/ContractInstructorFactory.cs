﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication.Models.ViewModels;
using WebApplication.Managers;

namespace WebApplication.Factory.FactoryMethod
{
    public class ContractInstructorFactory : BaseInstructorFactory
    {
        public ContractInstructorFactory(InstructorModel instructor)
            : base(instructor)
        {
        }

        public override IInstructorManager Create()
        {
            ContractInstructorManager manager = new ContractInstructorManager();
            _instructor.MedicalAllowance = manager.GetMedicalAllowance();
            return manager;
        }
    }
}