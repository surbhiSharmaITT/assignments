﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication.Models.ViewModels;
using Utilities;

namespace WebApplication.Factory.FactoryMethod
{
    public class InstructorManagerFactory
    {
        public BaseInstructorFactory CreateFactory(InstructorModel instructor)
        {
            BaseInstructorFactory returnValue = null;
            if (instructor.InstructorTypeId == Constants.PermanentInstructorTypeValue)
            {
                returnValue = new PermanentInstructorFactory(instructor);
            }
            else if (instructor.InstructorTypeId == Constants.ContractInstructorTypeValue)
            {
                returnValue = new ContractInstructorFactory(instructor);
            }
            return returnValue;
        }
    }
}