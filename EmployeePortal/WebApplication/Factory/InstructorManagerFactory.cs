﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication.Managers;


namespace WebApplication.Factory
{
    public class InstructorManagerFactory
    {
        public IInstructorManager GetEmployeeManager(int InstructorTypeId)
        {
            IInstructorManager returnValue = null;
            if (InstructorTypeId == 1)
            {
                returnValue = new PermanentInstructorManager();
            }
            else if (InstructorTypeId == 2)
            {
                returnValue = new ContractInstructorManager();
            }
            return returnValue;
        }
    }
}