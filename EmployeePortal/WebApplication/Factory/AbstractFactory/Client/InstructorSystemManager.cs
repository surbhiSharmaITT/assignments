﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication.Factory.AbstractFactory.AbstractInterface;
using WebApplication.Factory.AbstractFactory.AbstractProduct;

namespace WebApplication.Factory.AbstractFactory.Client
{
    public class InstructorSystemManager
    {
        readonly IComputerFactory _iComputerFactory;
        public InstructorSystemManager(IComputerFactory iComputerFactory)
        {
            _iComputerFactory = iComputerFactory;
        }
        public string GetSystemDetails()
        {
            IBrand brand = _iComputerFactory.Brand();
            IProcessor processor = _iComputerFactory.Processor();
            ISystemType systemType = _iComputerFactory.SystemType();
            string returnValue = string.Format("{0} {1} {2}", brand.GetBrand(),
                systemType.GetSystemType(), processor.GetProcessor());
            return returnValue;
        }
    }
}