﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication.Factory.AbstractFactory.AbstractProduct;

namespace WebApplication.Factory.AbstractFactory.ConcreteProduct
{
    public class Laptop : ISystemType
    {
        public string GetSystemType()
        {
            return Enumerations.ComputerTypes.Laptop.ToString();
        }
    }
    public class Desktop : ISystemType
    {
        public string GetSystemType()
        {
            return Enumerations.ComputerTypes.Desktop.ToString();
        }
    }
}