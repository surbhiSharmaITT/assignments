﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication.Factory.AbstractFactory.AbstractProduct;

namespace WebApplication.Factory.AbstractFactory.ConcreteProduct
{
    public class Dell : IBrand
    {
        public string GetBrand()
        {
            return Enumerations.Brands.DELL.ToString();
        }
    }

    public class Mac : IBrand
    {
        public string GetBrand()
        {
            return Enumerations.Brands.APPLE.ToString();
        }
    }
}