﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication.Factory.AbstractFactory.AbstractInterface;
using WebApplication.Models.ViewModels;
using Utilities;

namespace WebApplication.Factory.AbstractFactory.ConcreteFactory
{
    public class InstructorSystemFactory
    {
        public IComputerFactory Create(InstructorModel instructor)
        {
            IComputerFactory returnValue = null;
            if (instructor.InstructorTypeId == Constants.PermanentInstructorTypeValue)
            {
                if (instructor.Designation == "Manager")
                {
                    returnValue = new MacLaptopFactory();
                }
                else
                {
                    returnValue = new MacFactory();
                }
            }
            else if (instructor.InstructorTypeId == Constants.ContractInstructorTypeValue)
            {
                if (instructor.Designation == "Manager")
                {
                    returnValue = new DellLaptopFactory();
                }
                else
                    returnValue = new DellFactory();
            }
            return returnValue;
        }
    }
}