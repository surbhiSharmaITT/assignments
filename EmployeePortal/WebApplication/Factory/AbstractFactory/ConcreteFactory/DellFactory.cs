﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication.Factory.AbstractFactory.AbstractProduct;
using WebApplication.Factory.AbstractFactory.AbstractInterface;
using WebApplication.Factory.AbstractFactory.ConcreteProduct;

namespace WebApplication.Factory.AbstractFactory.ConcreteFactory
{
    public class DellFactory : IComputerFactory
    {
        public IBrand Brand()
        {
            return new Dell();
        }

        public IProcessor Processor()
        {
            return new I5();
        }

        public virtual ISystemType SystemType()
        {
            return new Desktop();
        }
    }
    public class DellLaptopFactory : DellFactory
    {
        public override ISystemType SystemType()
        {
            return new Laptop();
        }
    }
}