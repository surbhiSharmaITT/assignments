﻿using System;
using System.Collections.Generic;
using WebApplication.Models.Contracts;
using WebApplication.Models.ViewModels;
using System.Data.Entity;
using DB;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace WebApplication.Models.Repository
{
    public class StudentRepository : IStudentRepository
    {
        private StudentPortalApplicationEntities db = new StudentPortalApplicationEntities();

        /// <summary>
        /// It will fetch Student data in the form of list
        /// </summary>
        public IEnumerable<StudentModel> GetStudentData()
        {
            IEnumerable<StudentModel> students = null;
            try
            {
                students = (from student in db.Students
                            join course in db.Courses
                                on student.CourseId equals course.Id
                            select new StudentModel()
                            {
                                StudentId = student.StudentId,
                                FirstName = student.FirstName,
                                LastName = student.LastName,
                                Gender = student.Gender,
                                Address = student.Address,
                                CourseId = student.CourseId
                            }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return students;
        }

        /// <summary>
        /// It will fetch only a particlar record corresponding to the id
        /// </summary>
        public StudentModel GetStudentById(int studentId)
        {
            StudentModel studentValue = (from student in db.Students
                                         join course in db.Courses
                                            on student.CourseId equals course.Id
                                         where student.StudentId == studentId
                                         select new StudentModel()
                                         {
                                             StudentId = student.StudentId,
                                             FirstName = student.FirstName,
                                             LastName = student.LastName,
                                             Gender = student.Gender,
                                             Address = student.Address,
                                             CourseId = student.CourseId
                                         }).FirstOrDefault();
            return studentValue;
        }

        /// <summary>
        /// It will insert the data in the database
        /// </summary>
        public void InsertStudent(StudentModel student)
        {
            Student studentValue = new Student()
            {
                StudentId = student.StudentId,
                FirstName = student.FirstName,
                LastName = student.LastName,
                Gender = student.Gender,
                Address = student.Address,
                CourseId = student.CourseId
            };
            db.Students.Add(studentValue);
        }

        /// <summary>
        /// It will delete the data from the database
        /// </summary>
        public void DeleteStudent(StudentModel student)
        {
            Student studentValue = new Student()
            {
                StudentId = student.StudentId,
                FirstName = student.FirstName,
                LastName = student.LastName,
                Gender = student.Gender,
                Address = student.Address,
                CourseId = student.CourseId
            };
            db.Students.Attach(studentValue);
            db.Students.Remove(studentValue);
        }

        /// <summary>
        /// It will update the data in the database
        /// </summary>
        public void UpdateStudent(StudentModel student)
        {
            Student studentValue = new Student()
            {
                StudentId = student.StudentId,
                FirstName = student.FirstName,
                LastName = student.LastName,
                Gender = student.Gender,
                Address = student.Address,
                CourseId = student.CourseId
            };
            db.Entry(studentValue).State = EntityState.Modified;
        }

        /// <summary>
        /// Save the database
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }

        public void Disposed(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }

        /// <summary>
        /// Get select list for the InstructorType 
        /// </summary>
        public SelectList GetSelectList()
        {
            return new SelectList(db.Courses, "Id", "Title");
        }

        /// <summary>
        /// Get select list for the InstructorType on the basis of the InstructorModel object
        /// </summary>
        public SelectList GetSelectList(StudentModel student)
        {
            return new SelectList(db.Courses, "Id", "Title", student.CourseId);
        }
    }
}