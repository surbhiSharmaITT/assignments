﻿using System;
using System.Collections.Generic;
using WebApplication.Models.Contracts;
using WebApplication.Models.ViewModels;
using WebApplication.Factory.AbstractFactory.AbstractInterface;
using WebApplication.Factory.AbstractFactory.Client;
using WebApplication.Factory.AbstractFactory.ConcreteFactory;
using WebApplication.Factory.FactoryMethod;
using System.Data.Entity;
using DB;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace WebApplication.Models.Repository
{
    public class InstructorRepository : IInstructorRepository
    {
        private StudentPortalApplicationEntities db = new StudentPortalApplicationEntities();

        /// <summary>
        /// It will fetch instructor data in the form of list
        /// </summary>
        public IEnumerable<InstructorModel> GetInstructorData()
        {
            IEnumerable<InstructorModel> instructors = null;
            try
            {
                instructors = (from instructor in db.Instructors
                               join instructorType in db.InstructorTypes
                                   on instructor.InstructorTypeId equals instructorType.Id
                               join course in db.Courses
                                 on instructor.Course.Id equals course.Id
                               select new InstructorModel()
                               {
                                   InstructorId = instructor.InstructorId,
                                   FirstName = instructor.FirstName,
                                   LastName = instructor.LastName,
                                   Designation = instructor.Designation,
                                   Gender = instructor.Gender,
                                   Department = instructor.Department,
                                   HourlyPay = instructor.HourlyPay,
                                   Bonus = instructor.Bonus,
                                   InstructorTypeId = instructorType.Id,
                                   HouseAllowance = instructor.HouseAllowance,
                                   MedicalAllowance = instructor.MedicalAllowance,
                                   ComputerDetails = instructor.ComputerDetails
                               }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return instructors;
        }

        /// <summary>
        /// It will fetch only a particlar record corresponding to the id
        /// </summary>
        public InstructorModel GetInstructorById(int instructorId)
        {
            InstructorModel instructorValue = (from instructor in db.Instructors
                                               join course in db.Courses
                                                    on instructor.Course.Id equals course.Id
                                               join instructorType in db.InstructorTypes
                                                   on instructor.InstructorTypeId equals instructorType.Id
                                               where instructor.InstructorId == instructorId
                                               select new InstructorModel()
                                               {
                                                   InstructorId = instructor.InstructorId,
                                                   FirstName = instructor.FirstName,
                                                   LastName = instructor.LastName,
                                                   Designation = instructor.Designation,
                                                   Gender = instructor.Gender,
                                                   Department = instructor.Department,
                                                   HourlyPay = instructor.HourlyPay,
                                                   Bonus = instructor.Bonus,
                                                   InstructorTypeId = instructorType.Id,
                                                   HouseAllowance = instructor.HouseAllowance,
                                                   MedicalAllowance = instructor.MedicalAllowance,
                                                   ComputerDetails = instructor.ComputerDetails
                                               }).FirstOrDefault();
            return instructorValue;
        }

        /// <summary>
        /// It will insert the data in the database
        /// </summary>
        public void InsertInstructor(InstructorModel instructor)
        {
            BaseInstructorFactory instFactory = new InstructorManagerFactory().CreateFactory(instructor);
            instFactory.ApplySalary();
            IComputerFactory factory = new InstructorSystemFactory().Create(instructor);
            InstructorSystemManager manager = new InstructorSystemManager(factory);
            instructor.ComputerDetails = manager.GetSystemDetails();
            Instructor instructorValue = new Instructor()
            {
                InstructorId = instructor.InstructorId,
                FirstName = instructor.FirstName,
                LastName = instructor.LastName,
                Designation = instructor.Designation,
                Gender = instructor.Gender,
                Department = instructor.Department,
                HourlyPay = instructor.HourlyPay,
                Bonus = instructor.Bonus,
                InstructorTypeId = instructor.InstructorTypeId,
                HouseAllowance = instructor.HouseAllowance,
                MedicalAllowance = instructor.MedicalAllowance,
                ComputerDetails = instructor.ComputerDetails
            };
            db.Instructors.Add(instructorValue);
        }

        /// <summary>
        /// It will delete the data from the database
        /// </summary>
        public void DeleteInstructor(InstructorModel instructor)
        {
            Instructor instructorValue = new Instructor()
            {
                InstructorId = instructor.InstructorId,
                FirstName = instructor.FirstName,
                LastName = instructor.LastName,
                Designation = instructor.Designation,
                Gender = instructor.Gender,
                Department = instructor.Department,
                HourlyPay = instructor.HourlyPay,
                Bonus = instructor.Bonus,
                InstructorTypeId = instructor.InstructorTypeId,
                HouseAllowance = instructor.HouseAllowance,
                MedicalAllowance = instructor.MedicalAllowance,
                ComputerDetails = instructor.ComputerDetails
            };
            db.Instructors.Attach(instructorValue);
            db.Instructors.Remove(instructorValue);
        }

        /// <summary>
        /// It will update the data in the database
        /// </summary>
        public void UpdateInstructor(InstructorModel instructor)
        {
            Instructor instructorValue = new Instructor()
            {
                InstructorId = instructor.InstructorId,
                FirstName = instructor.FirstName,
                LastName = instructor.LastName,
                Designation = instructor.Designation,
                Gender = instructor.Gender,
                Department = instructor.Department,
                HourlyPay = instructor.HourlyPay,
                Bonus = instructor.Bonus,
                InstructorTypeId = instructor.InstructorTypeId,
                HouseAllowance = instructor.HouseAllowance,
                MedicalAllowance = instructor.MedicalAllowance,
                ComputerDetails = instructor.ComputerDetails
            };
            db.Entry(instructorValue).State = EntityState.Modified;
        }

        /// <summary>
        /// Save the database
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }

        public void Disposed(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }

        /// <summary>
        /// Get select list for the InstructorType 
        /// </summary>
        public SelectList GetSelectList()
        {
            return new SelectList(db.InstructorTypes, "Id", "InstructorTypeName");
        }

        /// <summary>
        /// Get select list for the InstructorType 
        /// </summary>
        public SelectList GetSelectListForCourses()
        {
            return new SelectList(db.Courses, "Id", "Title");
        } 

        /// <summary>
        /// Get select list for the InstructorType on the basis of the InstructorModel object
        /// </summary>
        public SelectList GetSelectList(InstructorModel instructor)
        {
            return new SelectList(db.InstructorTypes, "Id", "InstructorTypeName", instructor.InstructorTypeId);
        }

        /// <summary>
        /// Get select list for the InstructorType on the basis of the InstructorModel object
        /// </summary>
        public SelectList GetSelectListForCourses(InstructorModel instructor)
        {
            return new SelectList(db.Courses, "Id", "Title", instructor.InstructorId);
        }
    }
}