﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApplication.Models.ViewModels;

namespace WebApplication.Models.Contracts
{
    /// <summary>
    /// Interface for InstructorRepository
    /// </summary>
    public interface IInstructorRepository
    {
        IEnumerable<InstructorModel> GetInstructorData();
        InstructorModel GetInstructorById(int instructorId);
        void InsertInstructor(InstructorModel instructor);
        void DeleteInstructor(InstructorModel instructor);
        void UpdateInstructor(InstructorModel instructor);
        void Disposed(bool disposing);
        SelectList GetSelectList();
        SelectList GetSelectList(InstructorModel instructor);
        SelectList GetSelectListForCourses();
        SelectList GetSelectListForCourses(InstructorModel instructor);
        void Save();
    }
}
