﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebApplication.Models.ViewModels;

namespace WebApplication.Models.Contracts
{
    interface IStudentRepository
    {
        IEnumerable<StudentModel> GetStudentData();
        StudentModel GetStudentById(int instructorId);
        void InsertStudent(StudentModel instructor);
        void DeleteStudent(StudentModel instructor);
        void UpdateStudent(StudentModel instructor);
        void Disposed(bool disposing);
        SelectList GetSelectList();
        SelectList GetSelectList(StudentModel student);
        void Save();
    }
}
