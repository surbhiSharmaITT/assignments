﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models.ViewModels
{
    public class InstructorModel
    {
        public int InstructorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Designation { get; set; }
        public string Gender { get; set; }
        public string Department { get; set; }
        public decimal HourlyPay { get; set; }
        public decimal Bonus { get; set; }
        public int InstructorTypeId { get; set; }
        public Nullable<decimal> HouseAllowance { get; set; }
        public Nullable<decimal> MedicalAllowance { get; set; }
        public string ComputerDetails { get; set; }

        public virtual CourseModel Course { get; set; }
        public virtual InstructorTypeModel InstructorType { get; set; }
    }
}