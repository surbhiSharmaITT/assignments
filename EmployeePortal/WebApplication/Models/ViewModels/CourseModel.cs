﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models.ViewModels
{
    public class CourseModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CourseModel()
        {
            this.Students = new HashSet<StudentModel>();
        }
    
        public int Id { get; set; }
        public string Title { get; set; }
        public int Credit { get; set; }
        public int Duration { get; set; }
        public string Department { get; set; }
        public Nullable<int> InstructorId { get; set; }

        public virtual InstructorModel Instructor { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentModel> Students { get; set; }
    }
}