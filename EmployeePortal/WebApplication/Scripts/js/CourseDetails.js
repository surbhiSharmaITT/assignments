﻿/// <reference path="F:\Assignments/angular.js" />

(function () {
    angular.module('CourseApp')
    .controller("CourseController", function ($scope, CourseService) {
        $scope.Courses = null;
        $scope.CourseOrderBy = "Id";
        $scope.SortReverse = false;

        $scope.ShowCourses = function () {
            CourseService.GetCourses().then(function (response) {
                console.log(response.data);
                $scope.Courses = response.data;
                $scope.ShowDetails = true;
            }, function () {
                alert("Sorry! Error occured");
            });
        }

        $scope.SortCourses = function (orderBy) {
            if ($scope.CourseOrderBy == orderBy) {
                if ($scope.SortReverse == true) {
                    $scope.SortReverse = false;
                }
                else {
                    $scope.SortReverse = true;
                }
            }
            else {
                $scope.SortReverse = false;
            }
            $scope.CourseOrderBy = orderBy;
        }
    })
    .factory("CourseService", function ($http) {
        var CourseFactory = new Object();
        CourseFactory.GetCourses = function () {
            return $http.get("/Course/GetCourses");
        }
        return CourseFactory;
    });
})();