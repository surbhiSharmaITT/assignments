/// <autosync enabled="true" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="bootstrap.js" />
/// <reference path="respond.js" />
/// <reference path="angular.js" />
/// <reference path="angular-mocks.js" />
/// <reference path="angular-route.min.js" />
/// <reference path="datatables/responsive.semanticui.min.js" />
/// <reference path="jquery.datatables.min.js" />
/// <reference path="datatables.extensions/tabletools/js/dataTables.tableTools.min.js" />
/// <reference path="datatables.extensions/scroller/js/dataTables.scroller.min.js" />
/// <reference path="datatables.extensions/responsive/js/dataTables.responsive.min.js" />
/// <reference path="datatables.extensions/keytable/js/dataTables.keyTable.min.js" />
/// <reference path="datatables.extensions/fixedheader/js/dataTables.fixedHeader.min.js" />
/// <reference path="datatables.extensions/fixedcolumns/js/dataTables.fixedColumns.min.js" />
/// <reference path="datatables.extensions/colvis/js/dataTables.colVis.min.js" />
/// <reference path="datatables.extensions/colreorder/js/dataTables.colReorder.min.js" />
/// <reference path="datatables.extensions/autofill/js/dataTables.autoFill.min.js" />
/// <reference path="DataTables/autoFill.bootstrap.js" />
/// <reference path="DataTables/autoFill.bootstrap4.js" />
/// <reference path="DataTables/autoFill.foundation.js" />
/// <reference path="DataTables/autoFill.jqueryui.js" />
/// <reference path="DataTables/autoFill.semanticui.js" />
/// <reference path="DataTables/buttons.bootstrap.js" />
/// <reference path="DataTables/buttons.bootstrap4.js" />
/// <reference path="DataTables/buttons.colVis.js" />
/// <reference path="DataTables/buttons.flash.js" />
/// <reference path="DataTables/buttons.foundation.js" />
/// <reference path="DataTables/buttons.html5.js" />
/// <reference path="DataTables/buttons.jqueryui.js" />
/// <reference path="DataTables/buttons.print.js" />
/// <reference path="DataTables/buttons.semanticui.js" />
/// <reference path="DataTables/dataTables.autoFill.js" />
/// <reference path="DataTables/dataTables.bootstrap.js" />
/// <reference path="DataTables/dataTables.bootstrap4.js" />
/// <reference path="DataTables/dataTables.buttons.js" />
/// <reference path="DataTables/dataTables.colReorder.js" />
/// <reference path="DataTables/dataTables.fixedColumns.js" />
/// <reference path="DataTables/dataTables.fixedHeader.js" />
/// <reference path="DataTables/dataTables.foundation.js" />
/// <reference path="DataTables/dataTables.jqueryui.js" />
/// <reference path="DataTables/dataTables.keyTable.js" />
/// <reference path="DataTables/dataTables.material.js" />
/// <reference path="DataTables/dataTables.responsive.js" />
/// <reference path="DataTables/dataTables.rowGroup.js" />
/// <reference path="DataTables/dataTables.rowReorder.js" />
/// <reference path="DataTables/dataTables.scroller.js" />
/// <reference path="DataTables/dataTables.select.js" />
/// <reference path="DataTables/dataTables.semanticui.js" />
/// <reference path="DataTables/dataTables.uikit.js" />
/// <reference path="DataTables/jquery.dataTables.js" />
/// <reference path="DataTables/responsive.bootstrap.js" />
/// <reference path="DataTables/responsive.bootstrap4.js" />
/// <reference path="DataTables/responsive.foundation.js" />
/// <reference path="DataTables/responsive.jqueryui.js" />
/// <reference path="js/course.js" />
/// <reference path="js/coursedetails.js" />
