﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DB;
using WebApplication.Models.Repository;
using WebApplication.Models.Contracts;
using WebApplication.Models.ViewModels;

namespace WebApplication.Controllers
{
    public class InstructorsController : Controller
    {
        private IInstructorRepository instructorRepository = new InstructorRepository();

        // GET: Instructors
        public ActionResult Index()
        {
            var instructors = instructorRepository.GetInstructorData();
            return View(instructors);
        }

        // GET: Instructors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InstructorModel instructor = instructorRepository.GetInstructorById((int)id);
            if (instructor == null)
            {
                return HttpNotFound();
            }
            return View(instructor);
        }

        // GET: Instructors/Create
        public ActionResult Create()
        {
            ViewBag.InstructorId = instructorRepository.GetSelectListForCourses();
            ViewBag.InstructorTypeId = instructorRepository.GetSelectList();
            return View();
        }

        // POST: Instructors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InstructorId,FirstName,LastName,Designation,Gender,Department,InstructorTypeId")] InstructorModel instructor)
        {
            if (ModelState.IsValid)
            {
                instructorRepository.InsertInstructor(instructor);
                instructorRepository.Save();
                return RedirectToAction("Index");
            }

            ViewBag.InstructorId = instructorRepository.GetSelectListForCourses(instructor);
            ViewBag.InstructorTypeId = instructorRepository.GetSelectList(instructor);
            return View(instructor);
        }

        // GET: Instructors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InstructorModel instructor = instructorRepository.GetInstructorById((int)id);
            if (instructor == null)
            {
                return HttpNotFound();
            }
            ViewBag.InstructorId = instructorRepository.GetSelectListForCourses(instructor);
            ViewBag.InstructorTypeId = instructorRepository.GetSelectList(instructor);
            return View(instructor);
        }

        // POST: Instructors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InstructorId,FirstName,LastName,Designation,Gender,Department,HourlyPay,Bonus,InstructorTypeId,HouseAllowance,MedicalAllowance,ComputerDetails")] InstructorModel instructor)
        {
            if (ModelState.IsValid)
            {
                instructorRepository.UpdateInstructor(instructor);
                instructorRepository.Save();
                return RedirectToAction("Index");
            }
            ViewBag.InstructorId = instructorRepository.GetSelectListForCourses(instructor);
            ViewBag.InstructorTypeId = instructorRepository.GetSelectList(instructor);
            return View(instructor);
        }

        // GET: Instructors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InstructorModel instructor = instructorRepository.GetInstructorById((int)id);
            if (instructor == null)
            {
                return HttpNotFound();
            }
            return View(instructor);
        }

        // POST: Instructors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InstructorModel instructor = instructorRepository.GetInstructorById(id);
            instructorRepository.DeleteInstructor(instructor);
            instructorRepository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            instructorRepository.Disposed(disposing);
            base.Dispose(disposing);
        }
    }
}
