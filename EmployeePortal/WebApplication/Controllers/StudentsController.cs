﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DB;
using WebApplication.Models.ViewModels;
using WebApplication.Models.Contracts;
using WebApplication.Models.Repository;

namespace WebApplication.Controllers
{
    public class StudentsController : Controller
    {
        private IStudentRepository studentRepository = new StudentRepository();

        // GET: Students
        public ActionResult Index()
        {
            var students = studentRepository.GetStudentData();
            return View(students);
        }

        // GET: Students/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentModel student = studentRepository.GetStudentById((int)id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // GET: Students/Create
        public ActionResult Create()
        {
            ViewBag.CourseId = studentRepository.GetSelectList();
            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "StudentId,FirstName,LastName,Gender,Address,CourseId")] StudentModel student)
        {
            if (ModelState.IsValid)
            {
                studentRepository.InsertStudent(student);
                studentRepository.Save();
                return RedirectToAction("Index");
            }

            ViewBag.CourseId = studentRepository.GetSelectList(student);
            return View(student);
        }

        // GET: Students/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentModel student = studentRepository.GetStudentById((int)id);
            if (student == null)
            {
                return HttpNotFound();
            }
            ViewBag.CourseId = studentRepository.GetSelectList(student);
            return View(student);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "StudentId,FirstName,LastName,Gender,Address,CourseId")] StudentModel student)
        {
            if (ModelState.IsValid)
            {
                studentRepository.UpdateStudent(student);
                studentRepository.Save();
                return RedirectToAction("Index");
            }
            ViewBag.CourseId = studentRepository.GetSelectList(student);
            return View(student);
        }

        // GET: Students/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentModel student = studentRepository.GetStudentById((int)id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StudentModel student = studentRepository.GetStudentById(id);
            studentRepository.DeleteStudent(student);
            studentRepository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            studentRepository.Disposed(disposing);
            base.Dispose(disposing);
        }
    }
}
