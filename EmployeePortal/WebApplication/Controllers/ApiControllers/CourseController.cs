﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DB;

namespace WebApplication.Controllers.ApiControllers
{
    public class CourseController : ApiController
    {
        StudentPortalApplicationEntities db = new StudentPortalApplicationEntities();

        //Course/GetCourses
        [HttpGet]
        [Route("Course/GetCourses")]
        public List<Course> GetCourses()
        {
            var CourseList = db.Courses.ToList();
            var CoursesObj = CourseList.Select(d => new
            Course
            {
                Id = d.Id,
                Title = d.Title,
                Credit = d.Credit,
                Department = d.Department,
                Duration = d.Duration
            }).ToList();
            return CoursesObj;
        }

    }
}
