﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace Logger
{
    //Sealed restricts the inheritance
    public sealed class Log : ILog
    {
        private Log()
        {
        }

        private static readonly Lazy<Log> instance = new Lazy<Log>(() => new Log());
        /// <summary>
        /// Read-only property of type Log which will return single instance
        /// </summary>
        public static Log GetInstance
        {
            get
            {
                return instance.Value;
            }
        }

        /// <summary>
        /// Method which will log all the exceptions in unique file
        /// </summary>
        public void LogException(string message)
        {

            string fileName = string.Format(Constants.FullLogFileName, Constants.LogFileName, DateTime.Now.ToString(Constants.DateFormat));
            string logFilePath = string.Format(@"{0}\{1}", AppDomain.CurrentDomain.BaseDirectory, fileName);
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(Constants.LineSeparation);
            sb.AppendLine(DateTime.Now.ToString());
            sb.AppendLine(message);
            using (StreamWriter writer = new StreamWriter(logFilePath, true))
            {
                writer.Write(sb.ToString());
                writer.Flush();
            }
        }

    }
}
