﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Web.Mvc;

namespace WebApplication.Contracts
{
    interface IEmployeeRespository
    {
        public IEnumerable<Employee> GetAllEmployees();
       
        public SelectList GetSelectListEmployeeType();
        
        public SelectList GetSelectListEmployeeTypeId(Employee employee);
        
        public Employee GetEmployeeById(int id);
        
        public void AddEmployee(Employee employee);
        
        public void UpdateEmployee(Employee employee);
        
        public void Save();
        
        public void DeleteEmployee(Employee employee);

        public void DisposeObject();
        
    }
}
