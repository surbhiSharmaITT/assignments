﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DAL;
using WebApplication.Repository;


namespace WebApplication.Controllers
{
    public class EmployeesController : BaseController
    {
        private EmployeeRepository _employeeRepository = new EmployeeRepository();
        // GET: Employees
        public ActionResult Index()
        {
            return View(_employeeRepository.GetAllEmployees());
        }

        // GET: Employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = _employeeRepository.GetEmployeeById((int)id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Employees/Create
        public ActionResult Create()
        {
            ViewBag.EmployeeTypeID = _employeeRepository.GetSelectListEmployeeType();
            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,JobDescription,Number,Department,HourlyPay,Bonus,EmployeeTypeID")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                if (employee.EmployeeTypeID == 1)
                {
                    employee.HourlyPay = 800;
                    employee.Bonus = 1000;
                }
                else if (employee.EmployeeTypeID == 2)
                {
                    employee.HourlyPay = 1200;
                    employee.Bonus = 500;
                }
                _employeeRepository.AddEmployee(employee);
                _employeeRepository.Save();
                return RedirectToAction("Index");
            }

            ViewBag.EmployeeTypeID = _employeeRepository.GetSelectListEmployeeTypeId(employee);
            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = _employeeRepository.GetEmployeeById((int)id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.EmployeeTypeID = _employeeRepository.GetSelectListEmployeeTypeId(employee);
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,JobDescription,Number,Department,HourlyPay,Bonus,EmployeeTypeID")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                _employeeRepository.UpdateEmployee(employee);
               _employeeRepository.Save();
                return RedirectToAction("Index");
            }
            ViewBag.EmployeeTypeID = _employeeRepository.GetSelectListEmployeeTypeId(employee);
            return View(employee);
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = _employeeRepository.GetEmployeeById((int) id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = _employeeRepository.GetEmployeeById((int) id);
            _employeeRepository.DeleteEmployee(employee);
            _employeeRepository.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
               _employeeRepository.DisposeObject();
            }
            base.Dispose(disposing);
        }
    }
}
