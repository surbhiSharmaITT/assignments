﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using System.Net;
using System.Web.Mvc;
using Utility;

namespace WebApplication.Repository
{
    public class EmployeeRepository
    {
        EmployeePortalEntities entities = new EmployeePortalEntities();
        public IEnumerable<Employee> GetAllEmployees()
        {
            var employees = entities.Employees.Include(e => e.Employee_Type);
            return employees.ToList();

        }

        public SelectList GetSelectListEmployeeType()
        {
            return new SelectList(entities.Employee_Type, Constants.IdString, Constants.EmployeeTypeString);
        }

        public SelectList GetSelectListEmployeeTypeId(Employee employee)
        {
            return new SelectList(entities.Employee_Type, Constants.IdString, Constants.EmployeeTypeString, employee.EmployeeTypeID);
        }

        public Employee GetEmployeeById(int id)
        {
            return this.entities.Employees.Find(id);
        }

        public void AddEmployee(Employee employee)
        {
            this.entities.Employees.Add(employee);
        }

        public void UpdateEmployee(Employee employee)
        {
            this.entities.Entry(employee).State = EntityState.Modified;
        }

        public void Save()
        {
            this.entities.SaveChanges();
        }

        public void DeleteEmployee(Employee employee)
        {
            entities.Employees.Remove(employee);
        }

        public void DisposeObject()
        {
            entities.Dispose();
        }
    }
}