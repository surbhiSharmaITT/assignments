﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Web.Mvc;

namespace WebApplication.Contracts
{
    public interface IEmployeeRespository
    {
        IEnumerable<Employee> GetAllEmployees();

        SelectList GetSelectListEmployeeType();

        SelectList GetSelectListEmployeeTypeId(Employee employee);

        Employee GetEmployeeById(int id);

        void AddEmployee(Employee employee);

        void UpdateEmployee(Employee employee);

        void Save();

        void DeleteEmployee(Employee employee);

        void DisposeObject();
    }
}
