﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication.Managers;

namespace WebApplication.Factory
{
    public class EmployeeManagerFactory
    {
        /// <summary>
        /// Method which will return the instance depending upon the business requirement
        /// </summary>
       public IEmployeeManager GetEmployeeManager(int employeeTypeID)
        {
            IEmployeeManager returnValue = null;
            if (employeeTypeID == 1)
            {
                returnValue = new PermanentEmployeeManager();
            }
            else if (employeeTypeID == 2)
            {
                returnValue = new ContractEmployeeManager();
            }
            return returnValue;
        }
    }
}