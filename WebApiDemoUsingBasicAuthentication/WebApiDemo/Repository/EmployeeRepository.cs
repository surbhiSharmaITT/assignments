﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using EmployeeDataAccessLayer;
using Utilities;
using WebApiDemo.Contracts;

namespace WebApiDemo.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        EmployeeDBEntities entities = new EmployeeDBEntities();
        public IEnumerable<Employee> GetAllEmployees()
        {
            return entities.Employees.ToList();

        }

        public IEnumerable<Employee> GetMaleEmployees()
        {
            return this.entities.Employees.Where(e => e.Gender.ToLower() == Constants.GenderOptionsMale).ToList();

        }

        public IEnumerable<Employee> GetFemaleEmployees()
        {
            return this.entities.Employees.Where(e => e.Gender.ToLower() == Constants.GenderOptionsFemale).ToList();

        }

        public Employee GetEmployeeById(int id)
        {
            return this.entities.Employees.FirstOrDefault(e => e.ID == id);

        }

        public void AddEmployee(Employee employee)
        {
            this.entities.Employees.Add(employee);
        }

        public void Save()
        {
            this.entities.SaveChanges();
        }

        public void DeleteEmployee(Employee employee)
        {
            entities.Employees.Remove(employee);
        }
    }
}