﻿using EmployeeDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using Utilities;
using WebApiDemo.BLL;
using WebApiDemo.Repository;

namespace WebApiDemo.Controllers
{
    public class EmployeesController : ApiController
    {
        readonly EmployeeRepository _employeeRepository = new EmployeeRepository();
        [BasicAuthentication]
        public HttpResponseMessage Get(string gender = "All")
        {
            string username = Thread.CurrentPrincipal.Identity.Name;

            switch (username.ToLower())
            {
                case Constants.GenderOptionsMale:
                    return Request.CreateResponse(HttpStatusCode.OK,
                        _employeeRepository.GetMaleEmployees());
                case Constants.GenderOptionsFemale:
                    return Request.CreateResponse(HttpStatusCode.OK,
                        _employeeRepository.GetFemaleEmployees());
                default:
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        Constants.GenderInvalidOptionError + gender + Constants.GenderInvalidInputError);
            }

        }
        [HttpGet]
        public HttpResponseMessage LoadEmployeesById(int id)
        {
            var entity = _employeeRepository.GetEmployeeById(id);
            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                   Constants.LoadedIdNotFoundError + id.ToString());
            }

        }

        public HttpResponseMessage Post([FromBody] Employee employee)
        {
            try
            {
                _employeeRepository.AddEmployee(employee);
                _employeeRepository.Save();
                var message = Request.CreateResponse(HttpStatusCode.Created, employee);
                message.Headers.Location = new Uri(Request.RequestUri +
                    employee.ID.ToString());

                return message;

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        //public HttpResponseMessage Put([FromBody]int id, [FromUri]Employee employee)
        public HttpResponseMessage Put(int id, [FromBody] Employee employee)
        {
            try
            {
                var entity = _employeeRepository.GetEmployeeById(id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        Constants.UpdatedIdNotFoundError + id.ToString());
                }
                else
                {
                    entity.FirstName = employee.FirstName;
                    entity.LastName = employee.LastName;
                    entity.Gender = employee.Gender;
                    entity.Salary = employee.Salary;

                    _employeeRepository.Save();

                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var entity = _employeeRepository.GetEmployeeById(id);
                if (entity == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        Constants.DeletedIdNotFoundError + id.ToString());
                }
                else
                {
                    _employeeRepository.DeleteEmployee(entity);
                    _employeeRepository.Save();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


    }
}
