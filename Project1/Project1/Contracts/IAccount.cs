﻿namespace Project1.Contracts
{
    public interface IAccount
    {
       
        //Check the limit while transaction
        bool CheckLimit();

        //Completes the transaction
        void Transaction(decimal amount);

    }
}
