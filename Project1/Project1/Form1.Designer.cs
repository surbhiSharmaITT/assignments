﻿namespace Project1
{
    partial class CustomerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IdLabel = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.EmailIdLabel = new System.Windows.Forms.Label();
            this.AmountLabel = new System.Windows.Forms.Label();
            this.IdTextBox = new System.Windows.Forms.TextBox();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.EmailTextBox = new System.Windows.Forms.TextBox();
            this.AmountTextBox = new System.Windows.Forms.TextBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.DepositRadioButton = new System.Windows.Forms.RadioButton();
            this.WithdrawRadioButton = new System.Windows.Forms.RadioButton();
            this.ResultTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // IdLabel
            // 
            this.IdLabel.AutoSize = true;
            this.IdLabel.Location = new System.Drawing.Point(13, 37);
            this.IdLabel.Name = "IdLabel";
            this.IdLabel.Size = new System.Drawing.Size(18, 13);
            this.IdLabel.TabIndex = 0;
            this.IdLabel.Text = "ID";
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(13, 62);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(35, 13);
            this.NameLabel.TabIndex = 1;
            this.NameLabel.Text = "Name";
            // 
            // EmailIdLabel
            // 
            this.EmailIdLabel.AutoSize = true;
            this.EmailIdLabel.Location = new System.Drawing.Point(13, 86);
            this.EmailIdLabel.Name = "EmailIdLabel";
            this.EmailIdLabel.Size = new System.Drawing.Size(41, 13);
            this.EmailIdLabel.TabIndex = 2;
            this.EmailIdLabel.Text = "EmailId";
            // 
            // AmountLabel
            // 
            this.AmountLabel.AutoSize = true;
            this.AmountLabel.Location = new System.Drawing.Point(12, 111);
            this.AmountLabel.Name = "AmountLabel";
            this.AmountLabel.Size = new System.Drawing.Size(43, 13);
            this.AmountLabel.TabIndex = 3;
            this.AmountLabel.Text = "Amount";
            // 
            // IdTextBox
            // 
            this.IdTextBox.Location = new System.Drawing.Point(182, 37);
            this.IdTextBox.Name = "IdTextBox";
            this.IdTextBox.Size = new System.Drawing.Size(234, 20);
            this.IdTextBox.TabIndex = 4;
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(182, 62);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(234, 20);
            this.NameTextBox.TabIndex = 5;
            // 
            // EmailTextBox
            // 
            this.EmailTextBox.Location = new System.Drawing.Point(182, 86);
            this.EmailTextBox.Name = "EmailTextBox";
            this.EmailTextBox.Size = new System.Drawing.Size(234, 20);
            this.EmailTextBox.TabIndex = 6;
            // 
            // AmountTextBox
            // 
            this.AmountTextBox.Location = new System.Drawing.Point(182, 111);
            this.AmountTextBox.Name = "AmountTextBox";
            this.AmountTextBox.Size = new System.Drawing.Size(234, 20);
            this.AmountTextBox.TabIndex = 7;
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(96, 194);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(252, 23);
            this.SaveButton.TabIndex = 8;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // DepositRadioButton
            // 
            this.DepositRadioButton.AutoSize = true;
            this.DepositRadioButton.Location = new System.Drawing.Point(53, 160);
            this.DepositRadioButton.Name = "DepositRadioButton";
            this.DepositRadioButton.Size = new System.Drawing.Size(61, 17);
            this.DepositRadioButton.TabIndex = 9;
            this.DepositRadioButton.TabStop = true;
            this.DepositRadioButton.Text = "Deposit";
            this.DepositRadioButton.UseVisualStyleBackColor = true;
            // 
            // WithdrawRadioButton
            // 
            this.WithdrawRadioButton.AutoSize = true;
            this.WithdrawRadioButton.Location = new System.Drawing.Point(233, 160);
            this.WithdrawRadioButton.Name = "WithdrawRadioButton";
            this.WithdrawRadioButton.Size = new System.Drawing.Size(70, 17);
            this.WithdrawRadioButton.TabIndex = 10;
            this.WithdrawRadioButton.TabStop = true;
            this.WithdrawRadioButton.Text = "Withdraw";
            this.WithdrawRadioButton.UseVisualStyleBackColor = true;
            // 
            // ResultTextBox
            // 
            this.ResultTextBox.Location = new System.Drawing.Point(72, 239);
            this.ResultTextBox.Name = "ResultTextBox";
            this.ResultTextBox.Size = new System.Drawing.Size(306, 20);
            this.ResultTextBox.TabIndex = 11;
            // 
            // CustomerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 304);
            this.Controls.Add(this.ResultTextBox);
            this.Controls.Add(this.WithdrawRadioButton);
            this.Controls.Add(this.DepositRadioButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.AmountTextBox);
            this.Controls.Add(this.EmailTextBox);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.IdTextBox);
            this.Controls.Add(this.AmountLabel);
            this.Controls.Add(this.EmailIdLabel);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.IdLabel);
            this.Name = "CustomerForm";
            this.Text = " CustomerForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label IdLabel;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label EmailIdLabel;
        private System.Windows.Forms.Label AmountLabel;
        private System.Windows.Forms.TextBox IdTextBox;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.TextBox EmailTextBox;
        private System.Windows.Forms.TextBox AmountTextBox;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.RadioButton DepositRadioButton;
        private System.Windows.Forms.RadioButton WithdrawRadioButton;
        private System.Windows.Forms.TextBox ResultTextBox;
    }
}

