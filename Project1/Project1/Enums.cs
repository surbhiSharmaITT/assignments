﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    class Enums
    {
        //Specifies the type of transaction whether deposit or withdrawl
        public enum TypeOfTransaction
        {
            Deposit = 1,
            Withdraw = 2
        }
    }
}
