﻿namespace Project1
{
    class Account
    {
        // Read-Write Property Balance of an account
        public static decimal Balance { get; set; }

        // Read-Write Property Type of the account
        public static int Type { get; set; }

        //Read-Write Property filePath  storing the path of file 
        public static string FilePath { get; set; }
    }
}
