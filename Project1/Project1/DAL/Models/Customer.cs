﻿
namespace Project1
{
    class Customer
    {
        //Private fields of the customer
        #region Feilds

        private int id;
        private string name;
        private string emailId;
        private decimal amount;

        #endregion

        // Read-Write Properties
         #region Properties

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string EmailId
        {
            get { return emailId; }
            set { emailId = value; }
        }
        public decimal Amount
        {
            get { return amount; }
            set { amount = value; }
        }

        #endregion 
    }
}
