﻿using System;
using System.IO;
using System.Windows.Forms;
using Project1.Contracts;
using static Project1.Create;

namespace Project1
{
    // Step 1: Create a callback delegate. The actual callback method
    // signature should match with the signature of this delegate.
    public delegate void AddDataCallback(String path);
    class AddData : Create, IAccount
    {
        private readonly Customer _customer;
        // Delegate to call when the the Thread function completes                 
        // computing the sum of numbers
        readonly AddDataCallback _callbackMethod;

        // Constructor to initialize the target number and the callback delegateinitialize
        public AddData(Customer customer, AddDataCallback callbackMethod)
        {
            this._customer = customer;
            this._callbackMethod = callbackMethod;
        }

        public AddData()
        {
            
        }

        /// <summary>
        /// Add the customer data in the file either by creating new file or updating old one
        /// </summary>
        /// <param name="customer">Object containing all the data value</param>
        public void Add()
        {
            //Call the method createFile of the base class
            Create create = new Create();

            bool newFileCreated = create.SearchFile(_customer);

            //Using IAccount interface to use its implemented method
            IAccount employee = new AddData();

            //Check if the new file is created
            if (newFileCreated == true)
            {
                //Amount cannot be withdrawn in first transaction
                if (Account.Type == (int) Enums.TypeOfTransaction.Withdraw)
                { 
                    throw new Exception("Cannot be completed");
                }

                //If transaction is of type deposited
                else
                {
                    //Inserting Data in newly created file
                    using (StreamWriter streamWriter = new StreamWriter(FilePath))
                    {
                        String input = $"{_customer.Id} {_customer.Name} {_customer.EmailId} {_customer.Amount}";
                        streamWriter.WriteLine(input);
                        MessageBox.Show("Data Inserted.");
                    }
                }
                
            }
            //If file is already presnt and data is to be modified
            else
            {
                using (StreamReader streamReader = new StreamReader(FilePath))
                {
                    String data = streamReader.ReadLine();
                    if (data != null)
                    {
                        String[] dataArray = data.Split(' ');
                        Account.Balance = Convert.ToDecimal(dataArray[3]);
                    }
                    //If amount does not exceed the limit
                    if (employee.CheckLimit())
                    {
                        employee.Transaction(_customer.Amount);
                    }
                    else
                    {
                        throw new Exception("Cannot be completed");
                    }
                }
                //Deleting old file
                File.Delete(FilePath);
                //Creating new one
                using (StreamWriter streamWriter = new StreamWriter(FilePath))
                {
                    String input = $"{_customer.Id} {_customer.Name} {_customer.EmailId} {Balance}";
                    streamWriter.WriteLine(input);
                    MessageBox.Show("Data Updated.");
                }
            }
            string path = FilePath;
            if (_callbackMethod != null)
            {
                _callbackMethod(path);
            }
        }

        /// <summary>
        /// Check whether the transaction can be completed or not by checking limit
        /// </summary>
        public bool CheckLimit()
        {
            bool result = false;
            if ((int) Enums.TypeOfTransaction.Deposit == Type)
            {
                if (Balance < Constants.MaxLimit)
                    result = true;
            }
            else if ((int) Enums.TypeOfTransaction.Withdraw == Type)
            {
                if (Balance > Constants.MinLimit)
                    result = true;
            }
            return result;
        }

        /// <summary>
        /// Completes the transaction depending upon the type of transaction
        /// </summary>
        /// <param name="amount">The amount to be transacted</param>
        public void Transaction( decimal amount)
        {
            if ((int)Enums.TypeOfTransaction.Deposit == Type)
            {
                Balance = Balance + amount;
            }
            else if ((int)Enums.TypeOfTransaction.Withdraw == Type)
            {
                Balance = Balance - amount;
            }

        }
    }
}
