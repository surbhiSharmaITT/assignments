﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1
{
    class Constants
    {
        public const string DirectoryPath = @"H:\Programs\Text Files";
        public const decimal MaxLimit = 100000;
        public const decimal MinLimit = 0;
    }
}
