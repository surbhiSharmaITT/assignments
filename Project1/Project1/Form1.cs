﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace Project1
{
    public partial class CustomerForm : Form
    {
        /// <summary>
        /// Initialize the form
        /// </summary>
        public CustomerForm()
        {
            InitializeComponent();
        }

        public static void PrintFilePath(String filePath)
        {
            MessageBox.Show("{filepath}");
            //Console.WriteLine("Sum of numbers is " + filePath);
        }

        /// <summary>
        /// Method definition for button click event
        /// </summary>
        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Check whether the amount textBox is empty
                //If empty then replace it with amount 0
                decimal? chkAmount = Convert.ToDecimal(AmountTextBox.Text);
                decimal afterCheckAmount = chkAmount ?? 0;

                // Check whether the ID textBox is empty
                //If empty then replace it with id -1
                //Then throw exception
                int? chkId = Convert.ToInt32(IdTextBox.Text);
                int afterCheckId = chkId ?? -1;
                if (afterCheckId == -1)
                {
                    throw new Exception("Id cannot be null");
                }

                //Initializes the Customer constructor
                Customer customer = new Customer()
                {
                    Amount = afterCheckAmount,
                    EmailId = Convert.ToString(EmailTextBox.Text),
                    Id = afterCheckId,
                    Name = Convert.ToString(NameTextBox.Text)
                };

                //Set the value of account property type
                if (DepositRadioButton.Checked == true)
                {
                    Account.Type = 1;
                }
                else if (WithdrawRadioButton.Checked == true)
                {
                    Account.Type = 2;
                }

                //Call method Add of class AddData
                //AddData.Add(customer);


                // Create an instance of callback delegate and to it's constructor
                // pass the name of the callback function (PrintSumOfNumbers)
                AddDataCallback callbackMethod = new AddDataCallback(PrintFilePath);

                // Create an instance of Number class and to it's constrcutor pass the target
                // number and the instance of callback delegate
                AddData addData = new AddData(customer,callbackMethod);

                // Create an instance of Thread class and specify the Thread function to invoke
                Thread T1 = new Thread(new ThreadStart(addData.Add));
                T1.Start();
            }
            catch (Exception exception)
            {
                //To catch exception if present
                ResultTextBox.Text = exception.Message;
            }
        }
    }
}
