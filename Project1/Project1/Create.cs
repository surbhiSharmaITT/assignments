﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Project1
{
    class Create : Search
    {
        /// <summary>
        /// Generates the unique file name each time
        /// </summary>
        /// <param name="context">Name to be prefixed in each file</param>
        public static string GenerateFileName(string context)
        {
            return context + "_" + DateTime.Now.ToString("yyyyMMdd") + "_" + Guid.NewGuid().ToString("N")+".txt";
        }

        /// <summary>
        /// Creates new file depending on whether file exists or not
        /// </summary>
        /// <param name="customer">Whose data is to be added</param>
        /// <returns>Return if the new file is created or not</returns>
        public new bool SearchFile(Customer customer)
        {
            bool result = false;
            //Invoke the base method SearchFile
            bool dataPresent = base.SearchFile(customer);
            if (dataPresent == false)
            {
                String textFile = Create.GenerateFileName("File");
                Create.FilePath = $"{Constants.DirectoryPath}\\{textFile}";
                File.Create(Create.FilePath).Close();
                MessageBox.Show("File Created.");
                result = true;
            }
            return result;
        }

    }
}
